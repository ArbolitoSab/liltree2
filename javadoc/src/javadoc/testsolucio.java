package javadoc;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testsolucio {

	@Test
	void test() {
		assertEquals(3, solucio.maintwo(5, 1, 1, 2, 4));
		assertEquals(4, solucio.maintwo(5, 0, 0, 2, 4));
		assertEquals(4, solucio.maintwo(5, 1, 0, 4, 4));
		assertEquals(8, solucio.maintwo(11, 1, 1, 8, 9));
		assertEquals(3, solucio.maintwo(10, 1, 1, 2, 4));
		assertEquals(5, solucio.maintwo(7, 4, 1, 5, 6));
		assertEquals(0, solucio.maintwo(3, 1, 1, 1, 1));
		assertEquals(1, solucio.maintwo(2, 0, 0, 1, 1));
		assertEquals(5, solucio.maintwo(9, 1, 1, 4, 6));
		assertEquals(7, solucio.maintwo(10, 1, 5, 8, 2));
		assertEquals(1, solucio.maintwo(5, 1, 4, 2, 4));
		assertEquals(0, solucio.maintwo(1, 0, 0, 0, 0));
	}

}
