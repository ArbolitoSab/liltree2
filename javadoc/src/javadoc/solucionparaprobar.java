package javadoc;

import java.util.Scanner;

public class solucionparaprobar {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int size = sc.nextInt();
		int[][] matriz = new int[size][size];
		rellenarmatriz(matriz);
		int numobstaculos = sc.nextInt();
		for (int p = 0; p < numobstaculos; p++) {
			int of = sc.nextInt();
			int oc = sc.nextInt();
			matriz[of][oc] = 5;
		}
		int iniciof = sc.nextInt();
		int inicioc = sc.nextInt();
		matriz[iniciof][inicioc] = 1;
		int destinof = sc.nextInt();
		int destinoc = sc.nextInt();
		matriz[destinof][destinoc] = 2;
		int mov = size * size;
		boolean flag = false;
		int res = 0;
		for (int i = 0; i < mov && !flag; i++) {
			flag = calculardistancia(matriz, iniciof, inicioc, i);
			res = i;
		}
		if (flag) {
			System.out.println(res);
		} else {
			System.out.println("No");
		}
	}

	public static void rellenarmatriz(int[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int y = 0; y < matriz[i].length; y++) {
				matriz[i][y] = 0;
			}
		}
		return;
	}

	public static boolean calculardistancia(int[][] matriz, int f, int c, int podermover) {
		if (!estoyDentro(f, c, matriz))
			return false;
		if (matriz[f][c] == 5)
			return false;
		if (matriz[f][c] == 2)
			return true;
		if (podermover == 0)
			return false;
		matriz[f][c] = 1;
		podermover--;
		return calculardistancia(matriz, f - 1, c, podermover) || calculardistancia(matriz, f + 1, c, podermover)
				|| calculardistancia(matriz, f, c - 1, podermover) || calculardistancia(matriz, f, c + 1, podermover)
				|| calculardistancia(matriz, f - 1, c - 1, podermover)
				|| calculardistancia(matriz, f - 1, c + 1, podermover)
				|| calculardistancia(matriz, f + 1, c - 1, podermover)
				|| calculardistancia(matriz, f + 1, c + 1, podermover);
	}

	public static boolean estoyDentro(int h, int v, int[][] matriz) {

		if (h < 0 || v < 0 || h >= matriz.length || v >= matriz[0].length) {
			return false;
		} else {
			return true;
		}
	}

	public static void imprimir_matriz(int[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int y = 0; y < matriz[i].length; y++) {
				System.out.print(matriz[i][y] + " ");
			}
			System.out.println();
		}
		return;
	}
}
